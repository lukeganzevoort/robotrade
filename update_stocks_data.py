
import json
from pathlib import Path
from datamanager import DataManager
import time

with open('stocks.json', 'r') as stocks_file:

    data = json.load(stocks_file)

for symbol in data:
    file_name = 'robotrade-cache/dividends/{}-dividends.json'.format(symbol)
    if(Path(file_name).is_file() is False):
        print('Updating {}'.format(symbol))
        DataManager.update_dividends(symbol)
        time.sleep(0.5)

for symbol in data:
    file_name = 'robotrade-cache/stock-monthly/{}-monthly.json'.format(symbol)
    if(Path(file_name).is_file() is False):
        print('Updating {} Monthly.'.format(symbol))
        DataManager.update_monthly_stocks(symbol)
        time.sleep(0.5)

# TODO CHANGE TO UPDATE AND VERIFY THE DATA IS CORRECT when its time