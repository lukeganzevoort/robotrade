import requests
import urllib.request
import time
from datetime import datetime
from bs4 import BeautifulSoup
from requests_html import HTMLSession
import logging

class SeekingAlpha(object):

    def __init__(self):
        pass

    @staticmethod
    def get_dividend(symbol):

        # Create the browser session.
        symbol = symbol.upper()
        url = ('https://seekingalpha.com/symbol/' + symbol + '/dividends/history')
        session = HTMLSession()
        head = {
            'User-Agent': 'Mozilla/5.0 (X11; Ubuntu; Linux x86_64; rv:75.0) Gecko/20100101 Firefox/75.0',
            'Cookie': 'machine_cookie=8584062083749; __tbc=%7Bjzx%7DlCp-P5kTOqotpgFeypItnFxbflMekyKN4Ps7d-w-j67o5pxV7nDJeOkhQ7QAydzfQt-Lz-2EUMbbZNaNlGLDncyFuKZYqNp5Mb-uTsPBC7HMXTlgqCbEhtVN1JIcbMrCUVgFAp6l8Mpn6PJS7yCpyA; __pat=-14400000; xbc=%7Bjzx%7DEmhafGOUOMH3-rIjcg2Fa4VVc1hOamAYqQTL4NODshnlcp87kSnA7bkFddnirZRr9kgvSpTju8hJ61BVTANGGXZKSyRL5KnKc57FVvSgjZrns2txQ-G-vXPNZpZFaohQ-vt0mqLddIo_A_mlVuq3aOqVsKOgmzsND59IzuNMhpH6sTMN24YONad7Z32-hebbiD32CWJf-DneZUxfqfQYYhcKjEGAmeJgcTUx30JDTyURJOS1FL1JA0_lV47WdzZnYJwMyF5paXU6AFiAcqlVpeMJNKy8WHiE8UunNfmM6F8xJSZJ-JwSjASz7zujTLFetdY_LzjkAxUYa7ZgtuUa_v8akyLYgPzCYrDYOyUP2QtWDKFPNqZ9rqFqHj0I7uvoY4k4MgJi2gJDBkIuKjeg5aG0LbvoApWp13ewrUqs0HCbU7NdmQxLZHewaHTF791Y4iQWIOR5ooPr3vkl4qM1l6solaQaglITrfqQuJFLJVg5NkT9aGU5OavW7mV8LupszCAn6xxlH8-vA1asRstV2LuqqLkxG-dfgC6T80K9tegb64eE59s6-UFXxUKZGmRRjl_CjEiBZ2MJmi6dk5R9wVnG0JTevzBS2uqvt1COHdrDvSTnrmTl6km5crIXOt0w09OGwEaUGeyAr14-_WLc5y5GnjW4qHJUJ6x7bB_Zt-bnKbsWHuaOq-xcTAcxDi7A4MGxNc4KP_VlF51uH9OeBw; _pxvid=05615f3d-7a85-11ea-9789-0242ac120009; __pvi=%7B%22id%22%3A%22v-2020-04-17-22-34-26-859-zgeM1PBi1BFmA2bD-5f280249bf7e72e8446fe3f1bdac2c80%22%2C%22domain%22%3A%22.seekingalpha.com%22%2C%22time%22%3A1587177360185%7D'}
        resp = session.get(url, headers=head)

        # Verify the GET request was successful.
        if(resp.status_code != 200):
            logging.info('Request not successful. Code:{} for {}'.format(resp.status_code, url))
            return(False)

        # Render the webpage with all the java script and what not.
        resp.html.render()

        # Parse the webpage with BeautifulSoup.
        parsed = BeautifulSoup(resp.html.html, "html.parser")

        # Catch any errors
        if("I am not a robot" in parsed.get_text()):
            raise ValueError("I'm a robot and recapcha is stopping me.")

        if(parsed.find(id="dividends__no-dividends") is not None):
            raise ValueError("{} stock has no dividends.".format(symbol))

        correct_url = False
        for lk in parsed.find_all('link', href=True):
            if( url in lk['href']):
                correct_url = True
                break

        if(correct_url is False):
            if(parsed.find(id="analysis-and-news__analysis-and-news") is not None):
                raise ValueError("No dividends tab.")
            else:
                raise ValueError("The link wasn't right and I don't know why.")

        # Find the history item in the webpage.
        history_container = parsed.find(id="history-container")

        if(history_container is None):
            logging.error("Couldn't find history container.")
            raise ValueError("No history container.")

        # Within that item find the first table.
        table = history_container.table.find_all('tr', 'row-content')

        if(type(table) is None):
            raise ValueError("No table in history container.")

        # Extract the dividend information.
        dividends = []
        for row in table[1:]:
            col = row.find_all('td')
            dividends.append(
                {'declare_date': col[1].text,
                'ex-div-date': col[2].text,
                'record-date': col[3].text,
                'pay-date': col[4].text,
                'frequency': col[5].text,
                'amount': col[6].text,
                'adj-amount': col[7].text})

        logging.info("Successfully received {} dividends for {}".format(len(dividends), symbol))
        return(dividends)


if __name__ == '__main__':
    div = SeekingAlpha.get_dividend('QQQ')
    print(len(div))
