"""
Copyright © 2020 Luke Ganzevoort

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from tradier import Tradier
from seekingalpha import SeekingAlpha
from hdf5tocks import HDF5tocks
import json
import time
from datetime import datetime
from pathlib import Path
import logging

class DataManager(object):
    
    def __init__(self):

        pass


    @staticmethod
    def update_stock_price(symbol, date):

        symbol = symbol.upper()

        logging.debug("Updating {} price on {}.".format(symbol, date))

        poll_time = datetime.utcnow()

        # Get the stock data from Tradier
        td = Tradier()
        stock_data = td.get_daily_historical_data(
            symbol=symbol,
            start_date_str=date,
            end_date=date)

        if((type(stock_data) is not list) or (len(stock_data) != 1)):
            logging.warn("Didn't get stock info correctly.")
            return(False)

        file_path = ('robotrade-cache/stock-daily/'
            + '{}-daily.json'.format(symbol))

        if(Path(file_path).is_file() is True):

            with open(file_path, 'r') as json_file:
                json_string = json.load(json_file)

        else:

            json_string = {}
            json_string['_header'] = {}
            json_string['_header']['symbol'] = symbol
            json_string['_header']['last-update'] = ''
            json_string['prices'] = {}

        json_string['prices'][stock_data[0]['date']] = stock_data[0]
        json_string['_header']['last-update'] = poll_time.strftime('%Y-%m-%dT%H:%M:%SZ')

        with open(file_path, 'w') as json_file:
            json.dump(json_string, json_file, indent=4, sort_keys=True)

        return(True)


    @staticmethod
    def get_stock_price(symbol, date):

        symbol = symbol.upper()

        logging.debug("Getting the price of {} on {}.".format(symbol, date))

        file_path = ('robotrade-cache/stock-daily/'
            + '{}-daily.json'.format(symbol))

        if(Path(file_path).is_file() is not True):
            logging.warn('File does not exist.')
            return(False)

        with open(file_path, 'r') as json_file:
            data = json.load(json_file)

        if(date not in data['prices']):
            logging.warn('Information for that date is not available.')
            return(False)

        return(data['prices'][date])


    @staticmethod
    def update_monthly_stocks(symbol):

        symbol = symbol.upper()

        logging.debug("Updating the monthly stock price for {}.".format(symbol))

        # Get the stock data from Tradier
        poll_time = datetime.utcnow()
        td = Tradier()
        stock_data = td.get_monthly_historical_data(
            symbol=symbol,
            start_date="1800-01-01")

        if((type(stock_data) is not list) or (len(stock_data) == 0)):
            logging.warn("Didn't get stock info correctly.")
            return(False)

        file_path = ('robotrade-cache/stock-monthly/'
            + '{}-monthly.json'.format(symbol))

        if(Path(file_path).is_file() is True):

            with open(file_path, 'r') as json_file:
                json_string = json.load(json_file)

        else:

            json_string = {}
            json_string['_header'] = {}
            json_string['_header']['symbol'] = symbol
            json_string['_header']['last-update'] = ''
            json_string['prices'] = {}

        for month_data in stock_data:
            date = month_data['date']
            if((date not in json_string['prices'])
                    and (date != poll_time.strftime('%Y-%m-01'))):
                json_string['prices'][date] = month_data
        json_string['_header']['last-update'] = poll_time.strftime('%Y-%m-%dT%H:%M:%SZ')

        with open(file_path, 'w') as json_file:
            json.dump(json_string, json_file, indent=4, sort_keys=True)

        return(True)


    @staticmethod
    def update_dividends(symbol):

        symbol = symbol.upper()

        for retry in range(3):

            poll_time = datetime.utcnow()

            try:
                dividends = SeekingAlpha.get_dividend(symbol)

            except ValueError as e:
                if("I'm a robot" in str(e)):
                    print("Got the robot webpage. Retrying.")
                    time.sleep(3.5)
                    continue
                elif("has no dividends" in str(e)):
                    dividends = []
                elif("No dividends tab" in str(e)):
                    dividends = []
                else:
                    raise ValueError(str(e))

            if(type(dividends) is list):

                file_path = ('robotrade-cache/dividends/'
                    + '{}-dividends.json'.format(symbol))

                json_string = {}
                json_string['_header'] = {}
                json_string['_header']['symbol'] = symbol
                json_string['_header']['last-update'] = poll_time.strftime('%Y-%m-%dT%H:%M:%SZ')
                dividends.reverse()
                json_string['dividends'] = dividends

                with open(file_path, 'w') as json_file:
                    json.dump(json_string, json_file, indent=4, sort_keys=True)

                return(True)

        return(False)


    @staticmethod
    def get_dividends(symbol):

        symbol = symbol.upper()

        file_path = ('robotrade-cache/dividends/'
            + '{}-dividends.json'.format(symbol))

        if(Path(file_path).is_file() is True):

            with open(file_path, 'r') as json_file:
                data = json.load(json_file)
                return(data)

        DataManager.update_dividends(symbol)

        if(Path(file_path).is_file() is True):

            with open(file_path, 'r') as json_file:
                data = json.load(json_file)
                return(data)

        print('File does not exist.')
        return(False)


    @staticmethod
    def update_chains(symbol):

        print(f"Updating {symbol} Option Chains")
        logging.debug(f"Updating {symbol} Option Chains")

        td = Tradier()

        symbol = symbol.upper()

        file_path = f"cache-options-daily/{symbol}-options.hdf5"

        h5 = HDF5tocks(file_path)

        expirations = td.get_options_expirations(symbol)

        # h5.update_expirations(expirations)

        poll_time = time.time()
        quote = td.get_stock_data(symbol)
        quote['timestamp'] = poll_time

        h5.add_quote(quote)

        status = {
            'exp_total':len(expirations),
            'exp':0,
            'opt_cnt':0,
            'opt_in_exp_total':0,
            'opt_in_exp':0,
            'created':0,
            'updated':0,
            'no change':0,
            'new':0
        }

        for expiration in expirations:

            status['exp'] += 1
            status['opt_in_exp_total'] = 0
            status['opt_in_exp'] = 0
            status['created'] = 0
            status['updated'] = 0
            status['no change'] = 0
            status['new'] = 0
            chain_status = f"Getting chains for {expiration} ({status['exp']}/{status['exp_total']}). "
            print(chain_status, end="")
            logging.debug(f"Getting chains for {expiration}.")

            # Get the option data from Tradier
            poll_time = time.time()
            try:
                option_chain = td.get_options_chain(
                    symbol=symbol,
                    expiration=expiration)
            except:
                print("Oops... error. Trying again in 5 seconds.")
                logging.debug("Oops... error. Trying again in 5 seconds.")
                time.sleep(5)
                option_chain = td.get_options_chain(
                    symbol=symbol,
                    expiration=expiration)

            if((type(option_chain) is not list) or (len(option_chain) == 0)):
                print("Didn't get option chain correctly.")
                logging.warn("Didn't get option chain correctly.")
                raise ValueError("Didn't get option chain correctly.")

            status['opt_in_exp_total'] = len(option_chain)

            for opt in option_chain:

                status['opt_cnt'] += 1
                status['opt_in_exp'] += 1
                print(("\r"
                        + chain_status
                        + f"Adding option {opt['symbol']}. "
                        + f"({str(status['opt_in_exp']).zfill(4)}"
                        + f"/{str(status['opt_in_exp_total']).zfill(4)})"
                    ).ljust(80), end="")
                logging.debug(f"Adding option {opt['symbol']}")

                opt['timestamp'] = poll_time

                ret_val = h5.add_option(opt)

                if('created' in ret_val):
                    status['created'] += 1
                if('updated' in ret_val):
                    status['updated'] += 1
                elif('no change' in ret_val):
                    status['no change'] += 1
                elif('new' in ret_val):
                    status['new'] += 1
                else:
                    raise ValueError(f"I should have gotten 'updated', 'no change', or 'new' in {ret_val}.")

            print(f"\rUpdated {symbol.rjust(4)} {expiration} | "
                + f"Symbols:{str(status['opt_in_exp']).rjust(4)} "
                + f"(New:{str(status['created']).rjust(4)}) | "
                + f"Values:["
                + f"New:{str(status['new']).rjust(4)}, "
                + f"Chg:{str(status['updated']).rjust(4)}, "
                + f"NoChg:{str(status['no change']).rjust(4)}].")

        return(True)


if __name__ == '__main__':
    
    symbols = [
        'SPY',
        'QQQ', 
        'AAPL',
        'EEM',
        'XOM'
    ]

    for symbol in symbols:

        result = DataManager.update_stock_price(symbol, '2017-01-03')

        if(result is not True):
            print('Failed to update price for {}.'.format(symbol))

    for symbol in symbols:

        result = DataManager.get_stock_price(symbol, '2017-01-03')

        print('{} price on {} was {}.'.format(symbol, '2017-01-03', result['close']))


    for symbol in symbols:

        result = DataManager.update_dividends(symbol)

        if(result is not True):
            print('Failed to update {} dividends'.format(symbol))

    for symbol in symbols:

        result = DataManager.get_dividends(symbol)

        print('{} dividends last updated {}'.format(symbol, result['_header']['last-update']))
        print('Example entry {}'.format(result['dividends'][0]['pay-date']))

    symbols = [
        'SPY',
        'XLF'
    ]

    for symbol in symbols:
        DataManager.update_chains(symbol)


    