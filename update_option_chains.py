"""
Copyright © 2020 Luke Ganzevoort

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

"""
BUG:
Updated  SPY 2021-06-18 | Symbols: 276 (New:   0) | Values:[New: 276, Chg:   0, NoChg:   0].
Getting chains for 2021-06-30 (29/35). Adding option SPY210630P00215000. (0001/0236)Something when wrong with formating
{'symbol': 'SPY210630P00215000', 'description': 'SPY Jun 30 2021 $215.00 Put', 'exch': 'Z', 'type': 'option', 'last': None, 'change': None, 'volume': 0, 'open': None, 'high': None, 'low': None, 'close': None, 'bid': None, 'ask': None, 'underlying': 'SPY', 'strike': 215.0, 'change_percentage': None, 'average_volume': 0, 'last_volume': 0, 'trade_date': 0, 'prevclose': None, 'week_52_high': 0.0, 'week_52_low': 0.0, 'bidsize': 0, 'bidexch': None, 'bid_date': 0, 'asksize': 0, 'askexch': None, 'ask_date': 0, 'open_interest': 0, 'contract_size': 100, 'expiration_date': '2021-06-30', 'expiration_type': 'quarterlys', 'option_type': 'put', 'root_symbol': 'SPY', 'timestamp': 1593615862.8251703, 'sec_of_day': 5662}
Traceback (most recent call last):
  File "update_option_chains.py", line 34, in <module>
    DataManager.update_chains(symbol)
  File "/home/luke/Documents/robotrade/datamanager.py", line 309, in update_chains
    ret_val = h5.add_option(opt)
  File "/home/luke/Documents/robotrade/hdf5tocks.py", line 254, in add_option
    ds[-days_left] = self.format_option_entry(option)
  File "/home/luke/Documents/robotrade/hdf5tocks.py", line 355, in format_option_entry
    option_entry = np.array(option_list, dtype='i2')
UnboundLocalError: local variable 'option_list' referenced before assignment

"""


import json
from pathlib import Path
from datamanager import DataManager
import time
import logging

logging.basicConfig(
    level=logging.DEBUG,
    filename=f'logs/update_options_{time.strftime("%Y%m%d_%H%M%S", time.localtime())}.log',
    filemode='w',
    format='%(asctime)s - %(filename)s:%(lineno)d - %(levelname)s - %(message)s')

with open('./cache-options-daily/option-stocks.json', 'r') as stocks_file:

    data = json.load(stocks_file)

for symbol in data:

    DataManager.update_chains(symbol)
    time.sleep(0.5)
