"""
Copyright © 2020 Luke Ganzevoort

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import h5py

##
#   Instead of using this file, use the hdf5tocks.py. There are already read functions in there.
#

def get_option_expirations(symbol):

    option_hist = []

    with h5py.File(self.file_name, 'r+') as f:
        gp = f.get('options')
        ds = gp.get(symbol)
        for x in ds:
            option_hist.append(HDF5tocks.unformat_option_entry(x))
    return(option_hist)

    return exp_dates

def get_option_chain(symbol, expiration):

    return option_symbols

def get_option_strikes(symbol, expiration):

    return strike_prices

def get_option(symbol):

    return option_obj