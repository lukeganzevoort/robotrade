import sys
import datetime
import getpass
import robin_stocks as r

def get_username_and_password():
    if len(sys.argv) > 2:
        # Use the username and password from the terminal
        username = sys.argv[1]
        passwd = sys.argv[2]
    elif len(sys.argv) == 2:
        # Use the username from terminal, prompt for the password
        username = sys.argv[1]
        passwd = getpass.getpass("Password for " + sys.argv[1] + ":")
    else: # len(sys.argv) < 2
        # Prompt for the username and password
        username = input("Username:")
        passwd = getpass.getpass("Password for " + username + ":")
    return username, passwd

def rh_login(username="", password=""):
    try:
        # Login to robinhood. If a previous session is still available,
        # username and password can be anything and the previous session
        # authentication will be used.
        r.login( username, password)
    except:
        username, password = get_username_and_password()
        try: 
            # Try logging in with username and password.
            r.login( username, password)
        except:
            # Try again, but clear the access token. MFA will be re-requested.
            r.login( username, password, store_session=0)

symbol = 'qqq'
optionType = 'call'

rh_login()

# Get list of tradable options for the security
tradableOptions = r.options.find_tradable_options_for_stock( symbol, optionType=optionType, info=None)

# Get the threshold expiration date
tradewindow = datetime.date.today() + datetime.timedelta(+30)
tradeWindow = str(tradewindow)

# Get all expiration dates for the security
chain_info = r.options.get_chains( symbol)
expiration_dates = chain_info['expiration_dates']

# Filter the expiration dates not within the ideal trading window 

for date in sorted(expiration_dates):
    if date > tradeWindow:
        exp_date = date
        break

# Get all the options within the ideal trading window to idealOptions

idealOptions = r.options.find_options_for_stock_by_expiration( symbol, exp_date, optionType=optionType)

for option in idealOptions:
    option['market_data'] = r.options.get_option_market_data_by_id(option['id'])

theOption = ""
for option in sorted(idealOptions, key=lambda k: k['market_data']['ask_price'], reverse=True):
    if option['market_data']['ask_price'] < "0.50":
        theOption = option
        break
print(theOption['chain_symbol'],theOption['expiration_date'], theOption['strike_price'], theOption['type'])

# Place the limit buy
buyOption = r.orders.order_buy_option_limit(
    price = theOption['market_data']['ask_price'], 
    symbol = theOption['chain_symbol'], 
    quantity = 1, 
    expirationDate = theOption['expiration_date'], 
    strike = theOption['strike_price'], 
    optionType=theOption['type'], 
    timeInForce='gtc')

print("Order:", buyOption['time_in_force'], buyOption['opening_strategy'], buyOption['type'], "at", buyOption['price'])


# Place the limit sell
