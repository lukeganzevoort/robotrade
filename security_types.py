import requests
import re, json

from datetime import datetime, date, timedelta
from time import mktime, sleep
from io import StringIO
from urllib.parse import urlencode

from functools import wraps
from collections import defaultdict

import sys
# import datetime
import getpass
import robin_stocks as r

class Robinhood:

    def __init__(self):
        Robinhood.rh_login()

    @staticmethod
    def get_username_and_password():
        if len(sys.argv) > 2:
            # Use the username and password from the terminal
            username = sys.argv[1]
            passwd = sys.argv[2]
        elif len(sys.argv) == 2:
            # Use the username from terminal, prompt for the password
            username = sys.argv[1]
            passwd = getpass.getpass("Password for " + sys.argv[1] + ":")
        else: # len(sys.argv) < 2
            # Prompt for the username and password
            username = input("Username:")
            passwd = getpass.getpass("Password for " + username + ":")
        return username, passwd

    @staticmethod
    def rh_login(username="", password=""):
        try:
            # Login to robinhood. If a previous session is still available,
            # username and password can be anything and the previous session
            # authentication will be used.
            r.login( username, password)
        except:
            username, password = Robinhood.get_username_and_password()
            try: 
                # Try logging in with username and password.
                r.login( username, password)
            except:
                # Try again, but clear the access token. MFA will be re-requested.
                r.login( username, password, store_session=0)

class Chains:

    def __init__(self, ticker):
        self.symbol = ticker.upper()
        self.source = 'robinhood'
        Robinhood.rh_login()
        self.get_expirations()
        self.get_all_options()

    def get_expirations(self):
        # Get list of tradable options for the security
        temp = r.get_chains(self.symbol)
        assert(self.symbol == temp['symbol'])
        self.canOpenPosition = temp['can_open_position']
        self.tradeValueMultiplier = temp['trade_value_multiplier']
        self.instrumentURL = temp['underlying_instruments'][0]['instrument']
        self.instrumentID = temp['underlying_instruments'][0]['id']
        self.instrumentQnty = temp['underlying_instruments'][0]['quantity']
        self.expirationDates = sorted(temp['expiration_dates'])
        self.cashComponent = temp['cash_component']
        self.minTicksCutoffPrice = temp['min_ticks']['cutoff_price']
        self.minTicksBelowTick = temp['min_ticks']['below_tick']
        self.minTicksAboveTick = temp['min_ticks']['above_tick']
        self.chainID = temp['id']

    def get_all_options(self):
        self.options = []
        self.options = r.find_tradable_options_for_stock(self.symbol)
    
    def find_option_in_chain(self, expiration, strike, type):
        filtered_opts = filter(lambda x : float(x['strike_price']) == float(strike), self.options)
        filtered_opts = filter(lambda x : x['expiration_date'] == expiration, filtered_opts)
        filtered_opts = filter(lambda x : x['type'] == type, filtered_opts)
        filtered_opts = list(filtered_opts)
        assert(len(filtered_opts) == 1)
        return(filtered_opts[0]['id'])

class Option:

    def __init__(self, ticker="", strike="", optType="", expDate="", id=""):
        assert((ticker != "" and strike !="" and optType !="" and expDate !="") or (id !=""))
        self.symbol = ticker
        self.strike = strike
        self.type = optType
        self.expDate = expDate
        self.id = id
        self.source = 'robinhood'
        Robinhood.rh_login()

    def get_market_data(self):
        marketData = r.get_option_market_data_by_id(self.id)
        self.marketData = marketData
        self.ask = marketData['ask_price']
        self.askSize = marketData['ask_size']
        self.bid = marketData['bid_price']
        self.bidSize = marketData['bid_size']
        self.last = marketData['last_trade_price']

    def update(self):
        print("option updating")
        self.get_market_data()


class Stock:

    def __init__(self, ticker):
        Robinhood.rh_login()
        self.symbol = ticker
        self.source = 'robinhood'
        self.robinhood_fundamentals()
        self.robinhood_quote()
        self.robinhood_instrument()

    def update(self):
        self.source = self.source.lower()
        if self.source == 'robinhood':
            self.robinhood_quote()

    def robinhood_instrument(self):
        temp = r.get_instrument_by_url(self.instrumentURL)
        self.marginInitialRatio = temp['margin_initial_ratio']
        self.rhs_tradability = temp['rhs_tradability']
        self.id = temp['id']
        self.market = temp['market']
        self.simpleName = temp['simple_name']
        self.minTickSize = temp['min_tick_size']
        self.maintenanceRatio = temp['maintenance_ratio']
        self.tradability = temp['tradability']
        self.state = temp['state']
        self.type = temp['type']
        self.tradable = temp['tradeable']
        self.fundamentalsURL = temp['fundamentals']
        self.quoteURL = temp['quote']
        self.symbol = temp['symbol']
        self.defaultCollarFraction = temp['default_collar_fraction']
        self.dayTradeRatio = temp['day_trade_ratio']
        self.name = temp['name']
        self.tradableChainId = temp['tradable_chain_id']
        self.splits = temp['splits']
        self.country = temp['country']
        self.bloombergUnique = temp['bloomberg_unique']
        self.listDate = temp['list_date']

    def robinhood_quote(self):
        temp = r.get_stock_quote_by_symbol(self.symbol)
        assert(self.symbol == temp['symbol'])
        self.ask = temp['ask_price']
        self.askSize = temp['ask_size']
        self.bid = temp['bid_price']
        self.bidSize = temp['bid_size']
        self.last = temp['last_trade_price']
        self.lastExtHrs = temp['last_extended_hours_trade_price']
        self.prevClose = temp['previous_close']
        self.adjPrevClose = temp['adjusted_previous_close']
        self.prevCloseDate = temp['previous_close_date']
        self.tradingHalted = temp['trading_halted']
        self.hasTraded = temp['has_traded']
        self.lastTradeSource = temp['last_trade_price_source']
        self.updatedAt = temp['updated_at']
        self.updateTime = ((datetime.now() - datetime(1970,1,1)).total_seconds())

    def robinhood_fundamentals(self):
        temp = r.get_fundamentals(self.symbol)[0]
        self.open = float(temp['open'])
        self.high = float(temp['high'])
        self.low = float(temp['low'])
        self.volume = float(temp['volume'])
        self.avgVol2Wks = float(temp['average_volume_2_weeks'])
        self.avgVolume = float(temp['average_volume'])
        self.high52Wks = float(temp['high_52_weeks'])
        self.div_yield = float(temp['dividend_yield'])
        self.low52Wks = float(temp['low_52_weeks'])
        self.marketCap = float(temp['market_cap'])
        self.pbRatio = float(temp['pb_ratio'])
        self.peRatio = float(temp['pe_ratio'])
        self.outstandingShares = float(temp['shares_outstanding'])
        self.description = temp['description']
        self.instrumentURL = temp['instrument']
        self.ceo = temp['ceo']
        self.headquarters_city = temp['headquarters_city']
        self.headquarters_state = temp['headquarters_state']
        self.sector = temp['sector']
        self.industry = temp['industry']
        self.numEmployees = temp['num_employees']
        self.yearFounded = temp['year_founded']

    def historical(self, days_back=30, frequency='d'):
        pass

if __name__ == "__main__":
    s = Stock("QQQ")
    s.update()
    print(s.last,s.ask,s.bid)
    c = Chains("QQQ")
    t = (c.find_option_in_chain('2019-12-13', '200.00', 'call'))
    d = Option(id=t)
    d.get_market_data()
