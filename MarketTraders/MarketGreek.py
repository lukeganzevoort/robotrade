"""
Copyright © 2020 Luke Ganzevoort

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


# Import Standard Modules and Packages
import numpy as np

# Import Custom Module and Packages



class MarketGreek(object):

    def __init__(self, greek):
        self.greek = greek


    # -- Properties --
    @property
    def greek(self):
        return self._greek

    @greek.setter
    def greek(self, value):
        assert isinstance(value, (int, float)), f"greek {value} is not number"
        self._greek = value


    # -- Conversion Methods --
    def to_float16(self):
        greek_f16 = np.float16(self.greek)
        assert abs(greek_f16) != np.inf, f"Greek {self.greek} won't fit in float16"
        return greek_f16

    @staticmethod
    def from_float16(greek_float16):
        assert isinstance(greek_float16, np.float16), f"{greek_float16}"
        greek_float = float(greek_float16)
        return MarketGreek(greek_float)

    def to_float32(self):
        greek_f32 = np.float32(self.greek)
        assert abs(greek_f32) != np.inf, f"Greek {self.greek} won't fit in float32"
        return greek_f32

    @staticmethod
    def from_float32(greek_float32):
        assert isinstance(greek_float32, np.float32), f"{greek_float32}"
        greek_float = float(greek_float32)
        return MarketGreek(greek_float)
