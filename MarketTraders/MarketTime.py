"""
Copyright © 2020 Luke Ganzevoort

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


# Import Standard Modules and Packages
from datetime import datetime, timedelta

# Import Custom Module and Packages
import mcalendar as mcal



class MarketTime(object):

    market_open = 9.5

    def __init__(self, market_second, market_day, market_year):

        self.market_second = market_second
        self.market_day = market_day
        self.market_year = market_year


    @property
    def market_second(self):
        return self._market_second

    @market_second.setter
    def market_second(self, value):
        assert isinstance(value, int), value
        assert 0 <= value <= 23400, value
        self._market_second = value


    @property
    def market_day(self):
        return self._market_day

    @market_day.setter
    def market_day(self, value):
        assert isinstance(value, int), value
        assert 0 <= value < 253
        self._market_day = value


    @property
    def market_year(self):
        return self._market_year

    @market_year.setter
    def market_year(self, value):
        assert isinstance(value, int), value
        assert 1800 < value < 3000, value
        self._market_year = value


    @property
    def datetime(self):

        # Get date of market day
        cal = mcal.mcal_proxy.get_valid_days(f"{self.market_year}-01-01", f"{self.market_year}-12-31")
        date = cal[self.market_day]

        # Convert MarketTime to datetime
        dt = (datetime.strptime(date, "%Y-%m-%d")
            + timedelta(seconds=(self.market_second + (self.market_open * 3600))))
        return dt


    @property
    def timestamp(self):

        return(self.datetime - datetime(1970,1,1)).total_seconds()


    @property
    def time_str(self):

        return(self.datetime.strftime("%Y-%m-%d %H:%M:%S"))



    @staticmethod
    def from_datetime(dt):

        # Validate inputs
        assert isinstance(dt, datetime), datetime
        assert (isinstance(MarketTime.market_open, (int, float)) 
            and 0 <= MarketTime.market_open < 24), (
            "Please set MarketTime.market_open = to a number representing "
            + "the hour of market open. (i.e. To set market open to 9:30AM "
            + "use: MarketTime.market_open = 9.5). "
            + f"Currently market_open = {MarketTime.market_open}")

        # Get market open time
        open_hour = int(MarketTime.market_open)
        open_min = int((MarketTime.market_open % 1) * 60)
        open_sec = int(((MarketTime.market_open * 60) % 1) * 60)

        # Get the second of day relative to market open time
        market_second = (dt - dt.replace(hour=open_hour, minute=open_min, second=open_sec)).total_seconds()

        # Get the market day of the year
        market_day_num, market_day = mcal.get_market_day_of_year(dt)
        assert market_day == True, f"Is not a market day {dt}"

        return MarketTime(market_second, market_day_num, dt.year)


    @staticmethod
    def from_epoch_timestamp(ts):

        # Validate inputs
        assert isinstance(ts, (int, float)) and 0 <= ts

        # Determine datetime
        dt = datetime(1970, 1, 1) + timedelta(seconds=ts)

        # Use datetime function to get MarketTime object
        return MarketTime.from_datetime(dt)


    @staticmethod
    def from_time(year, month, day, hour, min, sec):

        # Validate inputs
        assert isinstance(year, int), year
        assert 1800 <= year <= 3000, f"year must be a valid year, year:{year}."
        assert isinstance(month, int), month
        assert 0 < month <= 12, f"month must be 1 to 12, month:{month}"
        assert isinstance(day, int), day
        assert 0 < day <= 31, f"day must be 1 to 31, day:{day}"
        assert isinstance(hour, int), hour
        assert 0 <= hour < 24 , f"hour must be 0 to 23, hour:{hour}"
        assert isinstance(min, int), min
        assert 0 <= min < 60 , f"min must be 0 to 59, min:{min}"
        assert isinstance(sec, int), sec
        assert 0 <= sec < 60 , f"sec must be 0 to 59, sec:{sec}"

        # Determine datetime
        dt = datetime(year=year, month=month, day=day, hour=hour, minute=min, second=sec)

        # Use datetime function to get MarketTime object
        return MarketTime.from_datetime(dt)


    @staticmethod
    def from_time_str(time_str):

        # Validate inputs
        assert isinstance(time_str, str), time_str

        # Determine datetime
        dt = datetime.strptime(time_str, "%Y-%m-%d %H:%M:%S")

        # Use datetime function to get MarketTime object
        return MarketTime.from_datetime(dt)
