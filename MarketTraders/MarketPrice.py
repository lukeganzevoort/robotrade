"""
Copyright © 2020 Luke Ganzevoort

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


# Import Standard Modules and Packages
import numpy as np

# Import Custom Module and Packages



class MarketPrice(object):

    def __init__(self, price):
        self.price = price


    # -- Properties --
    @property
    def price(self):
        return self._price

    @price.setter
    def price(self, value):
        assert isinstance(value, (int, float)), f"Price {value} is not number"
        assert value >= 0, f"Price {value} can't be negative"
        self._price = value


    # -- Conversion Methods --
    def to_uint16(self):
        cents = round(self.price * 100)
        assert 0 <= cents < 2**16, f"Price {cents} won't fit in a uint16"
        return np.uint16(cents)

    @staticmethod
    def from_uint16(price_uint16):
        assert isinstance(price_uint16, np.uint16), price_uint16
        price_float = int(price_uint16) / 100
        return MarketPrice(price_float)

    def to_uint32(self):
        cents = round(self.price * 100)
        assert 0 <= cents < 2**32, f"Price {cents} won't fit in a uint32"
        return np.uint16(cents)

    @staticmethod
    def from_uint32(price_uint32):
        assert isinstance(price_uint32, np.uint32), price_uint32
        price_float = int(price_uint32) / 100
        return MarketPrice(price_float)
