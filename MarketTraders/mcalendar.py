"""
Copyright © 2020 Luke Ganzevoort

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

import pandas_market_calendars as mcal
from datetime import datetime
import time
import json
from pathlib import Path
import logging


class mcal_proxy(object):

    file_path = 'robotrade-cache/calendar.json'
    valid_cache = {}
    sched_cache = {}


    def __init__(self):
        pass


    @staticmethod
    def get_valid_days(start_date, end_date):

        key = start_date + end_date
        if(key in mcal_proxy.valid_cache):
            return(mcal_proxy.valid_cache[key])
        else:
            nyse = mcal.get_calendar('NYSE')
            cal = nyse.valid_days(start_date=start_date, end_date=end_date)
            mcal_proxy.valid_cache[key] = cal
            return(cal)


    @staticmethod
    def get_schedule(start_date, end_date):

        key = start_date + end_date
        if(key in mcal_proxy.sched_cache):
            return(mcal_proxy.sched_cache[key])
        else:
            nyse = mcal.get_calendar('NYSE')
            sched = nyse.schedule(start_date=start_date, end_date=end_date)
            mcal_proxy.sched_cache[key] = sched
            return(sched)


def get_market_day_of_year(end_date):

    if(isinstance(end_date, datetime)):
        end_date = end_date.strftime('%Y-%m-%d')

    if(isinstance(end_date, (int, float))):
        end_date = time.strftime('%Y-%m-%d', time.localtime(end_date))

    start_date = "{}-01-01".format(end_date[0:4])
    start = time.time()
    cal = mcal_proxy.get_valid_days(start_date=start_date, end_date=end_date)
    logging.debug(f"Poll time for market calendar: {time.time()-start}")
    day_num = len(cal)
    market_day = (end_date in cal)

    return(day_num, market_day)


def get_market_days_left(current_date, expiration_date):

    if(isinstance(current_date, datetime)):
        current_date = current_date.strftime('%Y-%m-%d')

    if(isinstance(current_date, (int, float))):
        current_date = time.strftime('%Y-%m-%d', time.localtime(current_date))

    if(isinstance(expiration_date, datetime)):
        expiration_date = expiration_date.strftime('%Y-%m-%d')

    if(isinstance(expiration_date, (int, float))):
        expiration_date = time.strftime('%Y-%m-%d', time.localtime(expiration_date))

    cal = mcal_proxy.get_valid_days(start_date=current_date, end_date=expiration_date)
    days_left = len(cal)

    return(days_left)


def get_market_second_of_day(time_stamp):

    # Get market close and open on the day of the time_stamp
    # datetime.now(pytz.timezone('America/New_York'))
    # TODO: Figure out what time it is in New York before passing it to this function
    time_stamp_dt = datetime.fromtimestamp(time_stamp)
    date_str = time_stamp_dt.strftime('%Y-%m-%d')
    a = mcal_proxy.get_schedule(start_date=date_str, end_date=date_str)
    if(len(a) == 0):
        raise ValueError(f"{date_str} is not a market day.")
    market_open = a.market_open[0].timestamp()
    market_close = a.market_close[0].timestamp()

    assert(market_open <= time_stamp), '{} is before market open'.format(time_stamp)
    assert(time_stamp <= market_close), '{} is after market close'.format(time_stamp)

    market_second = int(time_stamp - market_open)
    return(market_second)


def get_market_open_timestamp(time_stamp):

    # Get market open on the day of the time_stamp
    time_stamp_dt = datetime.fromtimestamp(time_stamp)
    date_str = time_stamp_dt.strftime('%Y-%m-%d')
    a = mcal_proxy.get_schedule(start_date=date_str, end_date=date_str)
    market_open = a.market_open[0].timestamp()
    return(market_open)


if __name__ == "__main__":
    print(get_market_day_of_year('2020-05-31'))
    print(get_market_day_of_year(time.time()))
    print(get_market_day_of_year(datetime(2020,5,31)))
    print(get_market_days_left('2020-05-07','2020-05-12'))
    print(get_market_second_of_day(time.time()-3600*1))
    print(get_market_second_of_day(datetime(2020,6,1,12,5,50).timestamp()))
    print(get_market_second_of_day(time.time()))