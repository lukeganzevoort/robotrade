## @file robosaver.py
#
# This module is designed to save stock prices every hour. If this script is
# run within 6 minutes of 9:30AM or any top-of-the-hour hour until 4PM, then
# it will save the stock bid and ask and the time in an hdf5 file, using only
# 32-bit unsigned integers to minimize disk space. The hdf5 files are predefined
# so that each symbol has it's own file with the appropriate year. This way the
# file can have the appropriate size when it's created.
#
# TODO: Allow to save options
# TODO: Store all the data in one hdf5 file (or maybe not)
# TODO: Write script to run this at the top of the hour

import h5py
import security_types
from datetime import datetime
import pytz
import pandas

def download_data(symbol):
    s = security_types.Stock(symbol)
    return s

def save_data(data, symbol, index):
    nowDt = datetime.now()
    file = (symbol + str(nowDt.year) + ".hdf5")

    with h5py.File(file, 'a') as f:
        gSymbol = f.require_group(symbol)
        dt = h5py.string_dtype(encoding='utf-8')
        gHeader = gSymbol.require_dataset('header', shape=(1,3), dtype=dt, exact=True)
        gHeader[0] = ['TIME','BID','ASK']
        gData = gSymbol.require_dataset('data', shape=(2340,3), dtype='i4')
        get_index_for_year
        gData[index] = [int(data.updateTime), int(float(data.bid) * 100), int(float(data.ask) * 100)]

def get_index_for_year(dateAndTime):

    transactionsPerDay = 8
    hourIndexes = (9.5, 10, 11, 12, 13, 14, 15, 16)


    # We need to find what business day of the year it is. All M,T,W,R,F are
    # considered business days, including holidays. This is a maximum of 262
    # business days (when there is a leap year) and a minimum of 260 business
    # days (when the year ends on a weekend). If the given date is not a
    # weekday, the function will return None.

    startDate = datetime( dateAndTime.year, 1, 1)
    endDate = dateAndTime
    calendar = pandas.bdate_range( start=startDate, end=endDate, freq='B')

    if( endDate.date() != calendar[-1].date()):
        return None

    dayIndex = (len(calendar) - 1)


    # To get the proper index, the hour of the day must be evaluated. The
    # time must be within 6 minutes of one of the following times and then
    # the corresponding index will be assigned to it. If the time is not within
    # 6 minutes of one of the following times, the function will return None.
    #
    # 0 - 9:30
    # 1 - 10:00
    # 2 - 11:00
    # 3 - 12:00
    # 4 - 1:00
    # 5 - 2:00
    # 6 - 3:00
    # 7 - 4:00

    dateHour = (dateAndTime.hour 
        + (dateAndTime.minute / 60)
        + (dateAndTime.second / 3600))
    if dateHour <= 9.4 or dateHour >= 16.1:
        return None
    for hourIdx in range(len(hourIndexes)):
        tempOffset = round(dateHour - hourIndexes[hourIdx], 5)
        if(tempOffset < 0.1):
            if(tempOffset > -0.1):
                break
            else:
                return None


    # Finally, the correct index for the year can be found. Each day there are
    # 8 possible indexes, so just multiply the business day of the year by the
    # total indexes per day, then add the hour offset and BAM! you have your
    # index for the year!

    yearIdx = ((dayIndex * transactionsPerDay) + hourIdx)
    return(yearIdx)

# Read the contents back to me for anything that has some data in it
def read_file(filename, symbol):
    with h5py.File(filename, 'r') as f:
        dataset = f[symbol]["data"]
        for i in range(len(dataset)):
            if dataset[i].any() > 0:
                print("index:", i, "contents:", dataset[i])


if __name__ == "__main__":
    nowDt = datetime.now()
    securities = ['SPY', 'QQQ']
    index = get_index_for_year(datetime.now())

    if index == None:
        print("Now is not the time")
    else:
        for security in securities:
            # Download Data from Robinhood
            stockData = download_data(security)
            # Save data
            save_data(stockData, security, index)
            # Read me the saved data from the file
            filename = str(security + str(nowDt.year) + ".hdf5")
            read_file( filename, security)
