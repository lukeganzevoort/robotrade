"""
Copyright © 2020 Luke Ganzevoort

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from tradier import Tradier
from seekingalpha import SeekingAlpha
from datamanager import DataManager
from datetime import datetime
from datetime import timedelta
import logging

def get_growth_ratio(symbol, start_date, end_date=''):
    t = Tradier()
    hist = t.get_monthly_historical_data(symbol, start_date, end_date)
    growth_ratio = (hist[-1]['close'] / hist[0]['open'])
    return growth_ratio

def get_percent_gain(symbol, start_date, end_date=''):
    ratio = get_growth_ratio(symbol, start_date, end_date)
    perc_gain = (ratio - 1) * 100
    return(perc_gain)

def get_dividends(symbol):
    div = DataManager().get_dividends(symbol)['dividends']
    return div

def total_return(symbol:str, start_date:datetime, end_date:datetime=datetime.now()) -> float:

    assert(type(symbol) is str)
    assert(type(start_date) is datetime)
    assert(type(end_date) is datetime)

    # Get open price on start_date and purchase one share.
    start_day, days_off = get_closest_day_data(symbol, start_date)
    start_date = datetime.strptime(start_day['date'], '%Y-%m-%d')
    open_price = start_day['open']

    if(days_off < -3):
        logging.warn('Warning: Start date before specified date.')
    elif(days_off > 3):
        logging.warn('Warning: Start date is the IPO.')

    total_shares = 1.0
    logging.info('Purchased 1 share on {} for {}'.format(start_date.strftime('%Y-%m-%d'), open_price))

    if(end_date < start_date):
        raise ValueError('The end date is before the start date')

    # Get Dividends.
    divids = get_dividends(symbol)
    assert(type(divids) == list)
    # TODO: Fix this error check. The dates are in the format M/D/YYYY, which is terrible.
    # print(sorted(divids, key=lambda x: x['ex-div-date'])[0])
    # print(divids[0])
    # if(divids != sorted(divids, key=lambda x: x['ex-div-date'])):
    #     raise ValueError('The dividends are in the wrong order. (not ascending)')

    # For each ex-div-date after start_date and before end_date, get the price
    # of the stock on the pay-date and purchase a fractional share.
    for div in divids:

        ex_div_date = datetime.strptime(div['ex-div-date'], '%m/%d/%Y')
        pay_date = datetime.strptime(div['pay-date'], '%m/%d/%Y')

        if((start_date <= ex_div_date <= end_date) and (pay_date <= end_date)):

            stock_day, days_off = get_closest_day_data(symbol, pay_date)

            if(-4 < days_off < 4):
                shares = (float(div['adj-amount']) * total_shares) / stock_day['open']
                total_shares += shares
                logging.info('Purchased {} shares on {} for {}'.format(shares, pay_date.strftime('%Y-%m-%d'), stock_day['open']))
            else:
                logging.warn('Error: Got the wrong date.')
                raise ValueError('Error: Got the wrong date.')

    # Get close price on end_date and multiply it by the number of shares.
    end_day, days_off = get_closest_day_data(symbol, end_date)

    if(not (-4 < days_off < 4)):
        logging.warn('Error: Got the wrong end date.')
        logging.warn(end_day)
        logging.warn(days_off)

    tot_return = ((end_day['close'] * total_shares) / open_price)

    assert(type(tot_return) is float)
    return tot_return


## Get the data from a specific date or the closest market date.
#
# Description: This function gets the stock information for the date specified.
#   If the stock information is not present on the date, the stock will look for
#   data for the closest date with stock data (in case the date provided was a 
#   holiday or weekend). If the requested date is before the stock IPO, the
#   function will return the data from the IPO day.
#
# @param symbol     The ticker symbol of the stock.
# @param date_request The date to get data for in the form "YYYY-MM-DD".
# @return           The stock data for the date.
# @return           The number of days off from the specified date.
def get_closest_day_data(symbol:str, date_request:datetime) -> (dict, int):

    assert(type(symbol) is str)
    assert(type(date_request) is datetime)

    t = Tradier()
    date_plus = date_request + timedelta(days=4)
    date_minus = date_request - timedelta(days=4)

    # Get the daily stock data for the requested day +/- 4 days.
    stock_data = t.get_daily_historical_data(
        symbol=symbol,
        start_date_str=date_minus.strftime('%Y-%m-%d'),
        end_date=date_plus.strftime('%Y-%m-%d'))

    if(stock_data is not None):

        # Find the closest date to the requested date.
        stock_data.reverse()
        data_from_closest_day = min(stock_data, key=lambda x: abs(
            (datetime.strptime(x['date'], '%Y-%m-%d')
            - date_request).days))
    
    else:

        # There wasn't any data from the time requested date +/- 4 days, so
        # we'll assume the requested date is before any data is available. Let's
        # get the first date that market data is available from our source.
        data_from_closest_day = t.get_monthly_historical_data(symbol, '1800-01-01')[0]

    # Get the number of days off from the requested date.
    delta_days = (datetime.strptime(data_from_closest_day['date'], '%Y-%m-%d')
        - date_request).days

    assert(type(data_from_closest_day) is dict)
    assert(type(delta_days) is int)
    return(data_from_closest_day, delta_days)


if __name__ == '__main__':

    logging.basicConfig(level=logging.WARN)

    symbol = 'EEM'
    total_ret = total_return(symbol, start_date=datetime(2019,1,7))
    print('total return {}: {}'.format(symbol, total_ret))
