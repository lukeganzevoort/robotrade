"""
Copyright © 2020 Luke Ganzevoort

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

from OptionName import OptionName
from mcalendar import mcal_proxy
import mcalendar as mcal
from datetime import datetime, timedelta


class OptionEntry(OptionName):

    def __init__(self, name, data, market_days_to_expiration):

        OptionName.__init__(self, name)
        self.data = data
        self.market_days_to_expiration = market_days_to_expiration


    @property
    def underlying_price(self):
        assert False, "not implemented"

    @property
    def days_to_expiration(self):
        assert False, "not implemented"

    @property
    def datetime(self):
        return(datetime.strptime(self.entry_date , "%Y-%m-%d")
            + timedelta(hours=9.5, seconds=self.second_of_day))

    @property
    def entry_date(self):
        cal = mcal.get_market_calendar(
            start_date=f"{int(self.expiration[0:4]) - 1 - (self.market_days_to_expiration // 250)}-01-01",
            end_date=self.expiration)
        entry_date = cal[-1-self.market_days_to_expiration]
        return entry_date

    @property
    def second_of_day(self):
        return self.data[0]

    @property
    def ask(self):
        return self.data[1]

    @property
    def ask_size(self):
        return self.data[2]

    @property
    def bid(self):
        return self.data[3]

    @property
    def bid_size(self):
        return self.data[4]

    @property
    def delta(self):
        return self.data[7]

    @property
    def gamma(self):
        return self.data[8]

    @property
    def theta(self):
        return self.data[13]

    @property
    def vega(self):
        return self.data[14]

    @property
    def rho(self):
        return self.data[11]

    @property
    def phi(self):
        return self.data[10]

    @property
    def bid_iv(self):
        return self.data[6]

    @property
    def mid_iv(self):
        return self.data[9]

    @property
    def ask_iv(self):
        return self.data[5]

    @property
    def smv_vol(self):
        return self.data[12]