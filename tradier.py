import logging
import http.client
import json

class Tradier(object):

    def __init__(self):
        super(Tradier, self).__init__()
        self.key = 'QEaYVaQXmfF0ULyiueuaVAAjrgvf'

    def make_request(self, param_url):
        connection = http.client.HTTPSConnection('sandbox.tradier.com', 443, timeout = 30)
        headers = {"Accept":"application/json",
                   "Authorization":"Bearer " + self.key}
        connection.request('GET', (param_url), None, headers)

        try:
            response = connection.getresponse()
            content = json.loads(response.read())
            return (content)
        except http.client.HTTPException:
            logging.error("Exception during request")
            return(None)

    def get_stock_data(self, symbol):
        url = ("/v1/markets/quotes?" + "symbols=" + symbol)
        stock_data = self.make_request(url)
        quote = stock_data['quotes']['quote']
        return(quote)

    def get_options_chain(self, symbol, expiration):
        url = ("/v1/markets/options/chains?" + 
            "symbol=" + symbol + 
            "&expiration=" + expiration +
            "&greeks=true")
        chain_data = self.make_request(url)['options']['option']
        return(chain_data)

    def get_options_expirations(self, symbol):
        url = ("/v1/markets/options/expirations?" + 
            "symbol=" + symbol)
        expirations = self.make_request(url)['expirations']['date']
        return(expirations)

    def get_calendar(self, year, month):
        url = ("/v1/markets/calendar?" +
            "month=" + str(month).zfill(2) +
            "&year=" + str(year))
        calendar = self.make_request(url)['calendar']['days']['day']
        return(calendar)

    def get_clock(self):
        url = "/v1/markets/clock"
        clock = self.make_request(url)["clock"]
        return(clock)

    def get_dividends(self, symbol):
        url = ("/beta/markets/fundamentals/dividends?" +
            "symbols=" + symbol)
        dividends = self.make_request(url)
        return(dividends)

    def get_daily_historical_data(self, symbol, start_date_str, end_date=''):
        url = ("/v1/markets/history?" + 
            "symbol=" + symbol + 
            "&interval=daily" + 
            "&start=" + start_date_str +
            (("&end=" + end_date) if end_date != '' else ''))
        result = self.make_request(url)
        if(result is None) or (result['history'] is None) or (result['history']['day'] is None):
            return(None)
        historical_data = result['history']['day']
        if(type(historical_data) is dict):
            historical_data = [historical_data]
        return(historical_data)

    def get_weekly_historical_data(self, symbol, start_date_str, end_date=''):
        url = ("/v1/markets/history?" + 
            "symbol=" + symbol + 
            "&interval=weekly" + 
            "&start=" + start_date_str +
            (("&end=" + end_date) if end_date != '' else ''))
        result = self.make_request(url)
        if(result is None) or (result['history'] is None) or (result['history']['day'] is None):
            return(None)
        historical_data = result['history']['day']
        if(type(historical_data) is dict):
            historical_data = [historical_data]
        return(historical_data)

    def get_monthly_historical_data(self, symbol, start_date, end_date=''):
        url = ("/v1/markets/history?" + 
            "symbol=" + symbol + 
            "&interval=monthly" + 
            "&start=" + start_date +
            (("&end=" + end_date) if end_date != '' else ''))
        result = self.make_request(url)
        if(result is None) or (result['history'] is None) or (result['history']['day'] is None):
            return(None)
        historical_data = result['history']['day']
        if(type(historical_data) is dict):
            historical_data = [historical_data]
        return(historical_data)


def internal_test():
    symbol = 'QQQ'
    tdr = Tradier()
    stock = tdr.get_stock_data(symbol)
    print(stock['symbol'], stock['last'])
    exp_dates = tdr.get_options_expirations(symbol)
    print(exp_dates[0], exp_dates[1], exp_dates[2])
    chain = tdr.get_options_chain(symbol, exp_dates[0])
    print(len(chain), 'total options. Ex:', chain[0]['symbol'], chain[0]['ask'])
    calendar = tdr.get_calendar(2020,1)
    print(calendar[5]['date'], calendar[5]['status'])
    day = tdr.get_daily_historical_data(symbol, "2019-06-01")
    print(day[0]['date'], day[0]['open'], day[0]['close'])
    week = tdr.get_weekly_historical_data(symbol, "2019-01-01")
    print(week[0]['date'], week[0]['open'], week[0]['close'])
    week = tdr.get_monthly_historical_data(symbol, "2019-01-01", "2020-01-04")
    print(week[0]['date'], week[0]['open'], week[0]['close'])


if __name__ == '__main__':
    #internal_test()
    td = Tradier()

    symbols = [
        "SPY",
        "USO",
        "QQQ",
        "AAPL",
        "IWM",
        "TSLA",
        "BAC",
        "MSFT",
        "HYG",
        "AMD",
        "VXX",
        "BA",
        "GLD",
        "XLF",
        "SLV",
        "EEM",
        "FB",
        "AMZN"]


    option_chain = td.get_options_chain(
                    symbol=symbols[0],
                    expiration="2020-08-28")

    min_max_greeks = {
        "delta": [abs(option_chain[0]["greeks"]["delta"])]*2,
        "gamma": [abs(option_chain[0]["greeks"]["gamma"])]*2,
        "theta": [abs(option_chain[0]["greeks"]["theta"])]*2,
        "vega": [abs(option_chain[0]["greeks"]["vega"])]*2,
        "rho": [abs(option_chain[0]["greeks"]["rho"])]*2,
        "phi": [abs(option_chain[0]["greeks"]["phi"])]*2,
        "bid_iv": [abs(option_chain[0]["greeks"]["bid_iv"])]*2,
        "mid_iv": [abs(option_chain[0]["greeks"]["mid_iv"])]*2,
        "ask_iv": [abs(option_chain[0]["greeks"]["ask_iv"])]*2,
        "smv_vol": [abs(option_chain[0]["greeks"]["smv_vol"])]*2}


    for symbol in symbols:

        exps = td.get_options_expirations(symbol)

        for exp in exps:
            option_chain = td.get_options_chain(
                            symbol=symbol,
                            expiration=exp)

            for opt in option_chain:

                for greek in opt['greeks']:

                    if(greek == 'updated_at'):
                        continue

                    if(not isinstance(opt['greeks'][greek], float)):
                        print(opt['greeks'][greek], greek)

                    if( not( -50 < opt['greeks'][greek] < 50)):
                        print(opt['greeks'][greek])

                    if(opt['greeks'][greek] < min_max_greeks[greek][0]):
                        min_max_greeks[greek][0] = opt['greeks'][greek]
                    elif(opt['greeks'][greek] > min_max_greeks[greek][1]):
                        min_max_greeks[greek][1] = opt['greeks'][greek]


    for g in min_max_greeks:
        print(f"{g:>10} {min_max_greeks[g][0]:10.6f} {min_max_greeks[g][1]:10.6f}")

## 2020-08-21
#      delta  -1.000000   1.000060
#      gamma  -0.000000   0.530753
#      theta  -5.930250   0.000000
#       vega   0.000020  17.898300
#        rho   0.000000  27.543000
#        phi -58.131700   0.000000
#     bid_iv   0.000000   2.588620
#     mid_iv   0.000000   2.626451
#     ask_iv   0.000000   3.967660
#    smv_vol  -1.856000   2.204000

## 2020-08-25
#      delta  -1.000000   1.000450
#      gamma  -0.000000   0.797978
#      theta  -9.602970   0.000395
#       vega   0.000020  18.109000
#        rho   0.000000  27.803800
#        phi -58.979100   0.028340
#     bid_iv   0.000000   2.565840
#     mid_iv   0.000000   3.059948
#     ask_iv   0.000000   3.991710
#    smv_vol   0.054000   2.675000

## 2020-08-25 absolute value
#     delta   0.000000   1.000450
#     gamma   0.000000   0.797978
#     theta  -0.000000  -0.451460
#      vega   0.000020  18.109000
#       rho   0.000000  27.803800
#       phi   0.000000 -17.931800
#    bid_iv   0.000000   2.565840
#    mid_iv   0.000000   3.059948
#    ask_iv   0.000000   3.991710
#   smv_vol   0.054000   2.675000

# np.float16(1.00097656).tobytes()
# float(np.frombuffer(np.float16(0).tobytes(), dtype=np.float16)[0])
