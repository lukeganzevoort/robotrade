
from security_types import Chains, Option
from tradier import Tradier
from datetime import date, timedelta

def get_probability_itm(option):
    delta = option['greeks']['delta']
    probability = (delta)
    return probability

def single_option_value(close_price, buy_opt):
    if(buy_opt['option_type'] == 'call'):
        value = max((close_price - buy_opt['strike']), 0)
    elif(buy_opt['option_type'] == 'put'):
        value = max((buy_opt['strike'] - close_price), 0)
    else:
        print('error')
    
    return(value)

def get_profit(close_price, buy_opt, sell_opt):
    debit = (buy_opt['ask'] - sell_opt['bid'])
    opt1_val = single_option_value(close_price, buy_opt)
    opt2_val = - single_option_value(close_price, sell_opt)
    payout = opt1_val + opt2_val
    profit = payout - debit
    return(profit)


def prob_of_range(low_opt, high_opt):
    prob1 = get_probability_itm(low_opt)
    prob2 = get_probability_itm(high_opt)
    diff = abs(prob2 - prob1)
    return(diff)

def profit_of_range(low_opt, high_opt, buy_opt, sell_opt):
    prof_at_low = get_profit(low_opt['strike'], buy_opt, sell_opt)
    prof_at_high = get_profit(high_opt['strike'], buy_opt, sell_opt)
    avg = (prof_at_low + prof_at_high) / 2
    return(avg)

def avg_profit(chain, buy_opt, sell_opt):
    chain.append({'strike':0.0, 'option_type':buy_opt['option_type'], 'greeks':{'delta':1.0}})
    chain.append({'strike':1E25, 'option_type':buy_opt['option_type'], 'greeks':{'delta':0.0}})
    chain = filter(lambda x: x['option_type'] == buy_opt['option_type'], chain)
    chain = sorted(chain, key = lambda x: x['strike'])
    total = 0.0
    total_prob = 0.0
    for i in range(len(chain) - 1):
        profit = profit_of_range(chain[i], chain[i+1], buy_opt, sell_opt)
        prob = prob_of_range(chain[i], chain[i+1])
        total += (profit * prob)
        total_prob += prob
    
    return(total)

def get_exp_within_days(tdr, max_days):
    expirations = sorted(tdr.get_options_expirations(symbol), reverse=True)
    for exp in expirations:
        if(exp <= str(date.today() + timedelta(days=max_days))):
            break
    return(exp)

def get_opt_by_delta(chain, delta):
    chain = sorted(chain, key= lambda x: x['greeks']['delta'])
    for idx in range(len(chain)):
        if chain[idx]['greeks']['delta'] >= delta:
            break
    delta_diff1 = abs(chain[idx]['greeks']['delta'] - delta)
    delta_diff2 = abs(chain[idx-1]['greeks']['delta'] - delta)
    if(delta_diff2 < delta_diff1):
        idx = idx-1
    return(chain[idx])

if __name__ == '__main__':
    symbol = 'SPY'
    tdr = Tradier()
    print('Stock Price:', tdr.get_stock_data(symbol)['last'])
    exp = get_exp_within_days(tdr, 45)
    chain = tdr.get_options_chain(symbol, exp)
    sell_opt = get_opt_by_delta(chain, 0.55)
    buy_opt = get_opt_by_delta(chain, 0.35)
    print('Sell', sell_opt['symbol'], 'for', sell_opt['bid'], 'delta', sell_opt['greeks']['delta'])
    print('Buy', buy_opt['symbol'], 'for', buy_opt['ask'], 'delta', buy_opt['greeks']['delta'])
    print(avg_profit(chain, buy_opt, sell_opt))

