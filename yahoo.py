import requests
import urllib.request
import time
from datetime import datetime
from bs4 import BeautifulSoup

def get_dividend(symbol, start=datetime(1970,1,1), end=datetime.now()):
    period1 = int(start.timestamp())
    period2 = int(end.timestamp())
    url = ('https://finance.yahoo.com/quote/' + symbol + '/history?'
        + 'period1=' + str(period1) + '&period2=' + str(period2) 
        + '&interval=div|split&filter=div&frequency=1d')
    raw_html = requests.get(url)
    tr_tags = BeautifulSoup(raw_html.text, "html.parser").find_all('tr')
    for el in tr_tags:
        td_tags = el.find_all('td')
        if (len(td_tags) == 2) and ('Dividend' in td_tags[1].text) and (len(td_tags[0].span.text) == 12): 
            date = td_tags[0].span.text
            pay = td_tags[1].strong.text
            date = datetime.strptime(date, '%b %d, %Y')
            pay = float(pay)
            print(date, pay)

if __name__ == '__main__':
    get_dividend('', datetime(2019,1,1))

