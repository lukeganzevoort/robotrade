"""
Copyright © 2020 Luke Ganzevoort

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""


class OptionName(object):


    def __init__(self, name: str):
        self.name = name

    def validate(self):
        assert self.symbol.isalnum(), f"OptionName in wrong format {self.name}"
        assert self.underlying.isalpha(), f"OptionName in wrong format {self.name}"
        assert self.expiration_short.isdigit(), f"OptionName in wrong format {self.name}"
        assert self.option_type.isalpha(), f"OptionName in wrong format {self.name}"
        assert isinstance(self.strike, float), f"OptionName in wrong format {self.name}"

    @property
    def symbol(self):
        return self.name

    @property
    def underlying(self):
        return self.name[:-15]

    @property
    def expiration_short(self):
        return self.name[-15:-9]

    @property
    def expiration(self):
        exp_s = self.expiration_short
        return f"20{exp_s[0:2]}-{exp_s[2:4]}-{exp_s[4:6]}"

    @property
    def option_type(self):
        return self.name[-9]

    @property
    def strike(self):
        return int(self.name[-8:])/1000