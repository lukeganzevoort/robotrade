"""
Copyright © 2020 Luke Ganzevoort

Permission is hereby granted, free of charge, to any person obtaining a copy of
this software and associated documentation files (the “Software”), to deal in
the Software without restriction, including without limitation the rights to
use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies
of the Software, and to permit persons to whom the Software is furnished to do
so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED “AS IS”, WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
"""

"""
TODO: BUG
Updating TSLA Option Chains
Updated 274 options total. 274/274 for 2020-05-22. Created 0. Updated 0. No Change 0. New 274.
Updated 534 options total. 260/260 for 2020-05-29. Created 0. Updated 0. No Change 0. New 260.
Updated 760 options total. 226/226 for 2020-06-05. Created 0. Updated 0. No Change 0. New 226.
Updated 1010 options total. 250/250 for 2020-06-12. Created 0. Updated 0. No Change 0. New 250.
Updated 1522 options total. 512/512 for 2020-06-19. Created 0. Updated 0. No Change 0. New 512.
Updated 1638 options total. 116/116 for 2020-06-26. Created 0. Updated 0. No Change 0. New 116.
Updated 1696 options total. 58/58 for 2020-07-02. Created 0. Updated 0. No Change 0. New 58.
Updated 2150 options total. 454/454 for 2020-07-17. Created 0. Updated 0. No Change 0. New 454.
Updated 2612 options total. 462/462 for 2020-08-21. Created 0. Updated 0. No Change 0. New 462.
Getting chains for 2020-09-18 (10/20). Adding option TSLA200918P00010000. (0003/0508)Traceback (most recent call last):
  File "update_option_chains.py", line 34, in <module>
    DataManager.update_chains(symbol)
  File "/home/luke/Documents/robotrade/datamanager.py", line 309, in update_chains
    ret_val = h5.add_option(opt)
  File "/home/luke/Documents/robotrade/hdf5tocks.py", line 240, in add_option
    ds[-days_left] = self.format_option_entry(option)
  File "/home/luke/Documents/robotrade/hdf5tocks.py", line 341, in format_option_entry
    option_entry = np.array(option_list, dtype='i2')
OverflowError: Python int too large to convert to C long
"""

import time
from datetime import datetime
from datetime import timedelta
import h5py
import logging
import json
import numpy as np
import mcalendar as mcal
import re
from typing import List, NamedTuple

#
#   Stock File (spy.hdf5)
#       History
#           Daily
#               Date, close, high, low, open, volume
#           Monthly
#               Date, close, high, low, open, volume
#       Dividends
#           Date, adj-amount, amount, declare_date, ex-div-date, frequency, pay-date, record-date
#       Options (list-names)
#           Date, ask, asksize, bid, bidsize, ask_iv, bid_iv, delta, gamma, mid_iv, phi, rho, smv_vol, theta, vega, updated
#           TODO: Add list of expirations
#           TODO: Check if we already have the chain logged (within maybe 10 minutes of 11AM for the day) so we don't have to poll it again.
#           DONE: Only use the right timeslot for the option chain info
#           TODO: Add group for each expiration
#




class OptionEntry(NamedTuple):
    symbol: str
    datetime: datetime
    entry_date: str
    second_of_day: int
    days_to_expiration: int
    underlying: str
    underlying_price: float
    expiration: str
    option_type: str
    strike: float
    ask: float
    ask_size: int
    bid: float
    bid_size: int
    delta: float = None
    gamma: float = None
    theta: float = None
    vega: float = None
    rho: float = None
    phi: float = None
    bid_iv: float = None
    mid_iv: float = None
    ask_iv: float = None
    smv_vol: float = None


    def validate(self):

        success = True

        if(not isinstance(self.symbol, str)):
            logging.debug(f"symbol isn't str. {self.symbol}")
            success = False
        if(re.compile("([A-Z]+)([0-9]{6})([A-Z]{1})([0-9]{8})").match(self.symbol) is None):
            logging.debug(f"symbol not in correct format. {self.symbol}")
            success = False

        if(not isinstance(self.datetime, datetime)):
            logging.debug(f"datetime isn't datetime. {self.datetime}")
            success = False
            # TODO: Validate datetime is same as market_day and second_of_day

        if(not isinstance(self.entry_date, str)):
            logging.debug(f"entry_date isn't str. {self.entry_date}")
            success = False
        if(re.compile("([0-9]{4})-([0-9]{2})-([0-9]{2})").match(self.entry_date) is None):
            logging.debug(f"entry_date not in correct format. {self.entry_date}")
            success = False

        if(not isinstance(self.second_of_day, int)):
            logging.debug(f"second_of_day isn't int. {self.second_of_day}")
            success = False
        if(not 0 <= self.second_of_day <= 6.5 * 3600):
            logging.debug(f"second_of_day isn't in range. {self.second_of_day}")
            success = False

        if(not isinstance(self.days_to_expiration, int)):
            logging.debug(f"days_to_expiration isn't int. {self.days_to_expiration}")
            success = False
        if(not 0 <= self.days_to_expiration <= 253*5):
            logging.debug(f"days_to_expiration isn't in range. {self.days_to_expiration}")
            success = False
            
        if(not isinstance(self.underlying, str)):
            logging.debug(f"underlying isn't str. {self.underlying}")
            success = False
        if(not (len(self.underlying) <= 6 
            and self.underlying.upper() == self.underlying
            and self.underlying.split()[0] == self.underlying)):
            logging.debug(f"underlying isn't in a ticker symbol. {self.underlying}")
            success = False

        if(not isinstance(self.underlying_price, float)):
            logging.debug(f"underlying_price is not float. {self.underlying_price}")
            success = False
        if(not 0 < self.underlying_price < 10000):
            logging.debug(f"underlying_price not in range. {self.underlying_price}")
            success = False
        
        if(not isinstance(self.expiration, str)):
            logging.debug(f"expiration isn't str. {self.expiration}")
            success = False
        if(not (len(self.expiration.split('-')) == 3
            and len(self.expiration.split('-')[0]) == 4
            and len(self.expiration.split('-')[1]) == 2
            and len(self.expiration.split('-')[2]) == 2
            and self.expiration.split('-')[0].isdigit()
            and self.expiration.split('-')[1].isdigit()
            and self.expiration.split('-')[2].isdigit())):
            logging.debug(f"expiration is not correct form. {self.expiration}")
            success = False

        if(not isinstance(self.option_type, str)):
            logging.debug(f"option_type isn't str. {self.option_type}")
            success = False
        if(not (self.option_type == "call"
            or self.option_type == "put")):
            logging.debug(f"option_type is not 'call' or 'put'. {self.option_type}")
            success = False

        if(not isinstance(self.strike, float)):
            logging.debug(f"strike isn't float. {self.strike}")
            success = False
        if(not 0 < self.strike < 10000):
            logging.debug(f"strike not in range. {self.strike}")
            success = False

        if(not isinstance(self.ask, float)):
            logging.debug(f"ask isn't float. {self.ask}")
            success = False
        if(not 0 <= self.ask < 1000):
            logging.debug(f"ask isn't in range. {self.ask}")
            success = False

        if(not isinstance(self.ask_size, int)):
            logging.debug(f"ask_size isn't int. {self.ask_size}")
            success = False
        if(not 0 <= self.ask < 65536):
            logging.debug(f"ask_size isn't in range. {self.ask_size}")
            success = False

        if(not isinstance(self.bid, float)):
            logging.debug(f"bid isn't float. {self.bid}")
            success = False
        if(not 0 <= self.bid < 1000):
            logging.debug(f"bid isn't in range. {self.bid}")
            success = False

        if(not isinstance(self.bid_size, int)):
            logging.debug(f"bid_size isn't int. {self.bid_size}")
            success = False
        if(not 0 <= self.bid_size < 65536):
            logging.debug(f"bid_size isn't in range. {self.bid_size}")
            success = False

        if(self.delta is not None):
            if(not isinstance(self.delta, float)):
                logging.debug(f"delta isn't float. {self.delta}")
                success = False
            if(not -1 <= self.delta <= 1):
                logging.debug(f"delta not in range. {self.delta}")
                success = False

        if(self.gamma is not None):
            if(not isinstance(self.gamma, float)):
                logging.debug(f"gamma isn't float. {self.gamma}")
                success = False
            if(not -1 <= self.gamma <= 1):
                logging.debug(f"gamma not in range. {self.gamma}")
                success = False

        if(self.theta is not None):
            if(not isinstance(self.theta, float)):
                logging.debug(f"theta isn't float. {self.theta}")
                success = False
            if(not -1000 < self.theta <= 0):
                logging.debug(f"theta not in range. {self.theta}")
                success = False

        if(self.vega is not None):
            if(not isinstance(self.vega, float)):
                logging.debug(f"vega isn't float. {self.vega}")
                success = False
            if(not -10 < self.vega < 10):
                logging.debug(f"vega not in range. {self.vega}")
                success = False

        if(self.rho is not None):
            if(not isinstance(self.rho, float)):
                logging.debug(f"rho isn't float. {self.rho}")
                success = False
            if(not -10 <= self.rho <= 10):
                logging.debug(f"rho not in range. {self.rho}")
                success = False

        if(self.phi is not None):
            if(not isinstance(self.phi, float)):
                logging.debug(f"phi isn't float. {self.phi}")
                success = False
            if(not -10 <= self.phi <= 10):
                logging.debug(f"phi not in range. {self.phi}")
                success = False

        if(self.bid_iv is not None):
            if(not isinstance(self.bid_iv, float)):
                logging.debug(f"bid_iv isn't float. {self.bid_iv}")
                success = False
            if(not 0 <= self.bid_iv < 10000):
                logging.debug(f"bid_iv not in range. {self.bid_iv}")
                success = False

        if(self.mid_iv is not None):
            if(not isinstance(self.mid_iv, float)):
                logging.debug(f"mid_iv isn't float. {self.mid_iv}")
                success = False
            if(not 0 <= self.mid_iv < 10000):
                logging.debug(f"mid_iv not in range. {self.mid_iv}")
                success = False

        if(self.ask_iv is not None):
            if(not isinstance(self.ask_iv, float)):
                logging.debug(f"ask_iv isn't float. {self.ask_iv}")
                success = False
            if(not 0 <= self.ask_iv < 10000):
                logging.debug(f"ask_iv not in range. {self.ask_iv}")
                success = False

        if(self.smv_vol is not None):
            if(not isinstance(self.smv_vol, float)):
                logging.debug(f"smv_vol isn't float. {self.smv_vol}")
                success = False
            if(not 0 <= self.smv_vol < 10000):
                logging.debug(f"smv_vol not in range. {self.smv_vol}")
                success = False

        return success


class HDF5tocks(object):


    def __init__(self, file_name):

        self.file_name = file_name
        self.history_labels = [
            'date',
            'close',
            'high',
            'low',
            'open',
            'volume'
        ]
        self.quotes = [
            'timestamp',
            'last',
            'bid',
            'ask'
        ]
        self.dividend_labels = [
            'date',
            'adj-amount',
            'amount',
            'declare-date',
            'ex-div-date',
            'frequency',
            'pay-date',
            'record-date',
        ]
        self.options_labels = [
            'timestamp',
            'ask',
            'asksize',
            'bid',
            'bidsize',
            'ask_iv',
            'bid_iv',
            'delta',
            'gamma',
            'mid_iv',
            'phi',
            'rho',
            'smv_vol',
            'theta',
            'vega'
        ]

        # Create file if needed
        with h5py.File(self.file_name, 'a'):
            pass

        # Create main groups and labels
        with h5py.File(self.file_name, 'r+') as f:
            gp = f.require_group('history')
            if(gp.get('labels') is None):
                gp.create_dataset('labels', data=[x.encode() for x in self.history_labels])
            gp = f.require_group('dividends')
            if(gp.get('labels') is None):
                gp.create_dataset('labels', data=[x.encode() for x in self.dividend_labels])
            gp = f.require_group('options')
            if(gp.get('labels') is None):
                gp.create_dataset('labels', data=[x.encode() for x in self.options_labels])


    def add_quote(self, quote):

        quote_dt = datetime.utcfromtimestamp(quote['timestamp'])
        quote_year = quote_dt.year
        quote_date = quote_dt.strftime('%Y-%m-%d')
        quote_sec_of_day = mcal.get_market_second_of_day(time_stamp=quote['timestamp'])
        quote['sec_of_day'] = quote_sec_of_day

        set_name = f'11am_quotes_{quote_year}'
        total_days = mcal.get_market_day_of_year(f'{quote_year}-12-31')[0]
        quote_day_idx, market_day = mcal.get_market_day_of_year(quote_date)
        quote_day_idx -= 1
        if(market_day is False):
            logging.debug("This isn't actually a market day, so set time to 0.")
            quote_sec_of_day = 0
        entry_len = len(self.quotes)

        with h5py.File(self.file_name, 'r+') as f:
            gp = f.get('history')
            ds = gp.get(set_name)
            if(ds is None):
                ds = gp.create_dataset(
                    name=set_name,
                    shape=(total_days, entry_len),
                    dtype='i2'
                )

            eleven = 1.5*3600
            time_in_hdf = int(HDF5tocks.int16_to_uint16(ds[quote_day_idx,0]))
            if(int(time_in_hdf) != 0):
                if(abs(time_in_hdf-eleven) > abs(quote_sec_of_day-eleven)):
                    # Closer to eleven
                    logging.debug("Closer to 11AM")
                    ds[quote_day_idx] = self.format_quote_entry(quote)
                else:
                    # Not closer than what we have in there already
                    logging.debug("Not closer to 11AM")
            else:
                # No entry yet
                logging.debug("No entry yet")
                ds[quote_day_idx] = self.format_quote_entry(quote)


    # def update_expirations(self, expirations):

    #     expirations = [x.encode() for x in expirations]

    #     with h5py.File(self.file_name, 'r+') as f:
    #         gp = f.get('options')
    #         ds = gp.get('expirations')
    #         if(ds is None):
    #             gp.create_dataset(
    #                 name='expirations',
    #                 data=expirations,
    #                 maxshape=(2000,)
    #             )
    #         else:
    #             ds_list = sorted(list(set(list(ds) + expirations)))
    #             ds.resize((len(ds_list),))
    #             ds[:] = ds_list


    # def get_expirations(self):

    #     with h5py.File(self.file_name, 'a') as f:
    #         gp = f.get('options')
    #         expirations = gp.get('expirations')[:]

    #     expirations = list(expirations.astype(str))

    #     return(expirations)


    def add_option(self, option):

        ret_val = []
        symbol = option['symbol']
        ret_val.append(symbol)
        exp_date = option['expiration_date']
        poll_time = option['timestamp']
        entry_len = len(self.options_labels)

        days_left = mcal.get_market_days_left(
            current_date=poll_time,
            expiration_date=exp_date
        )
        assert(days_left > 0), 'This option is in the past. Exp: {}'.format(exp_date)
        sec_of_day = mcal.get_market_second_of_day(time_stamp=poll_time)
        eleven = 1.5*3600
        option['sec_of_day'] = sec_of_day

        with h5py.File(self.file_name, 'r+') as f:
            gp = f.get('options')
            if(gp.get(symbol) is None):
                logging.debug(f"Creating Option {symbol}")
                ret_val.append("created")
                gp.create_dataset(
                    name=symbol,
                    shape=(days_left, entry_len),
                    dtype='i2')
            ds = gp.get(symbol)

            time_in_hdf = int(HDF5tocks.int16_to_uint16(ds[-days_left,0]))
            if(int(time_in_hdf) != 0):
                if(abs(time_in_hdf-eleven) > abs(sec_of_day-eleven)):
                    # Closer to eleven
                    logging.debug("Closer to 11AM")
                    ret_val.append("updated")
                    ds[-days_left] = self.format_option_entry(option)
                else:
                    # Not closer than what we have in there already
                    logging.debug("Not closer to 11AM")
                    ret_val.append("no change")
            else:
                # No entry yet
                logging.debug("No entry yet")
                ret_val.append("new")
                ds[-days_left] = self.format_option_entry(option)

        return(ret_val)


    def get_11am_stock_entry(self, date):

        quote_year = date[0:4]
        quote_day_idx, market_day = mcal.get_market_day_of_year(date)
        quote_day_idx -= 1
        if(market_day is False):
            logging.debug("This isn't actually a market day, getting previous market day.")
        
        set_name = f'11am_quotes_{quote_year}'
        with h5py.File(self.file_name, 'r+') as f:
            gp = f.get('history')
            data_set = gp.get(set_name)
            
            entry_dict = HDF5tocks.unformat_quote_entry(data_set[quote_day_idx])

        market_second_of_day = entry_dict['sec_of_day']
        EDT_second_of_day = (9.5 * 3600) + market_second_of_day

        start_date = f"{int(date[0:4])}-01-01"
        end_date = f"{date[0:4]}-12-31"
        calendar = mcal.mcal_proxy.get_valid_days(start_date, end_date)
        calendar = [x.strftime("%Y-%m-%d") for x in list(calendar)]
        this_day_date = calendar[quote_day_idx]
                        
        this_day_datetime = datetime.strptime(this_day_date, "%Y-%m-%d") + timedelta(seconds=EDT_second_of_day)


        stock_entry = StockEntry(
            symbol=self.file_name.split('-')[0],
            datetime=this_day_datetime,
            entry_date=this_day_date,
            second_of_day=market_second_of_day,
            last=entry_dict['last'],
            ask=entry_dict['ask'],
            bid=entry_dict['bid'],
        )

        return(stock_entry)


    def get_dataset(self, data_set_path):

        with h5py.File(self.file_name, 'r') as f:

            data_set = f.get(data_set_path)
            data_set_arr = np.zeros(data_set.shape, dtype=data_set.dtype)
            data_set.read_direct(data_set_arr)
        
        return data_set_arr


    def get_dataset_shape(self, data_set_path):

        with h5py.File(self.file_name, 'r') as f:

            data_set = f.get(data_set_path)
            data_set_shape = data_set.shape
        
        return data_set_shape


    def get_dataset_dtype(self, data_set_path):

        with h5py.File(self.file_name, 'r') as f:

            data_set = f.get(data_set_path)
            data_set_dtype = data_set.dtype
        
        return data_set_dtype


    def get_option_entry_on_date(self, symbol, date):

        temp = re.compile("([A-Z]+)([0-9]{6})([A-Z]{1})([0-9]{8})")
        res = temp.match(symbol).groups()
        exp_date = f"20{res[1][0:2]}-{res[1][2:4]}-{res[1][4:6]}"

        shape = self.get_dataset_shape(f"options/{symbol}")
        recorded_entries = shape[0]
        
        start_date = f"{int(exp_date[0:4])-5}-12-31"
        end_date = f"{int(exp_date[0:4])+5}-12-31"
        calendar = mcal.mcal_proxy.get_valid_days(start_date, end_date)
        calendar = [x.strftime("%Y-%m-%d") for x in list(calendar)]
        
        exp_idx = calendar.index(exp_date)
        date_idx = calendar.index(date)
        entry_idx = -1 - (exp_idx - date_idx)

        if not(-recorded_entries <= entry_idx < 0):
            logging.debug(f"Entry not recorded for date {date} {symbol}")
            return None

        with h5py.File(self.file_name, 'r') as f:

            data_set = f.get(f"options/{symbol}")
            data_set_arr = np.zeros((data_set.shape[1],), dtype=data_set.dtype)
            data_set.read_direct(data_set_arr, np.s_[entry_idx])

        return data_set_arr


    def get_option(self, symbol:str) -> List[OptionEntry]:

        option_hist = []

        temp = re.compile("([A-Z]+)([0-9]{6})([A-Z]{1})([0-9]{8})")
        res = temp.match(symbol)
        res = res.groups()
        underlying = res[0]
        expiration = f"20{res[1][0:2]}-{res[1][2:4]}-{res[1][4:6]}"
        option_type = 'call' if res[2] == 'C' else ''
        option_type = 'put' if res[2] == 'P' else option_type
        strike = int(res[3])/1000

        start_date = f"{int(expiration[0:4])-5}-12-31"
        end_date = f"{expiration[0:4]}-12-31"
        calendar = mcal.mcal_proxy.get_valid_days(start_date, end_date)
        calendar = [x.strftime("%Y-%m-%d") for x in list(calendar)]
        exp_idx = calendar.index(expiration)


        with h5py.File(self.file_name, 'r+') as f:
            gp = f.get('options')
            data_set = gp.get(symbol)
            market_days_in_data_set = len(data_set)

            for idx, x in enumerate(data_set):

                entry_dict = HDF5tocks.unformat_option_entry(x)
                market_second_of_day = entry_dict['sec_of_day']
                EDT_second_of_day = (9.5 * 3600) + market_second_of_day
                
                days_to_expiration = market_days_in_data_set - 1 - idx
                this_day_cal_idx = exp_idx - days_to_expiration
                this_day_date = calendar[this_day_cal_idx]
                underlying_entry = self.get_11am_stock_entry(this_day_date)

                this_day_datetime = datetime.strptime(this_day_date, "%Y-%m-%d") + timedelta(seconds=EDT_second_of_day)
                
                opt_entry = OptionEntry(
                    symbol=symbol,
                    datetime=this_day_datetime,
                    entry_date=this_day_date,
                    second_of_day=market_second_of_day,
                    days_to_expiration=days_to_expiration,
                    underlying=underlying,
                    underlying_price=underlying_entry.last,
                    expiration=expiration,
                    option_type=option_type,
                    strike=strike,
                    ask=entry_dict['ask'],
                    ask_size=entry_dict['asksize'],
                    bid=entry_dict['bid'],
                    bid_size=entry_dict['bidsize'],
                    delta=entry_dict['greeks']['delta'],
                    gamma=entry_dict['greeks']['gamma'],
                    theta=entry_dict['greeks']['theta'],
                    vega=entry_dict['greeks']['vega'],
                    rho=entry_dict['greeks']['rho'],
                    phi=entry_dict['greeks']['phi'],
                    bid_iv=entry_dict['greeks']['bid_iv'],
                    mid_iv=entry_dict['greeks']['mid_iv'],
                    ask_iv=entry_dict['greeks']['ask_iv'],
                    smv_vol=entry_dict['greeks']['smv_vol'],
                )

                option_hist.append(opt_entry)
        return(option_hist)


    def get_all_options(self) -> List[List[OptionEntry]]:

        with h5py.File(self.file_name, 'r+') as f:
            gp = f.get('options')
            keys = list(gp.keys())

        temp = re.compile("([A-Z]+)([0-9]{6})([A-Z]{1})([0-9]{8})")
        option_list = []

        for opt_name in keys:

            res = temp.match(opt_name)
            if res is None:
                continue

            option_list.append(opt_name)

        return(option_list)


    def get_options_expirations(self) -> List[str]:

        expirations = set()

        with h5py.File(self.file_name, 'r+') as f:
            gp = f.get('options')
            keys = list(gp.keys())

        for key in keys:

            temp = re.compile("([A-Z]+)([0-9]{6})([A-Z]{1})([0-9]{8})")
            res = temp.match(key)
            if res is None:
                continue

            symbol_name_split = res.groups()
            expiration = symbol_name_split[1]
            expiration_full = f"20{expiration[0:2]}-{expiration[2:4]}-{expiration[4:6]}"

            expirations.add(expiration_full)

        expirations = sorted(expirations)

        return(expirations)


    def get_strikes(self, expiration:str) -> List[float]:

        strikes = set()

        with h5py.File(self.file_name, 'r+') as f:
            gp = f.get('options')
            keys = list(gp.keys())

        exp_short = expiration[2:].replace('-', '')

        for key in keys:

            temp = re.compile("([A-Z]+)([0-9]{6})([A-Z]{1})([0-9]{8})")
            res = temp.match(key)
            if res is None:
                continue

            symbol_name_split = res.groups()
            exp = symbol_name_split[1]
            strike = int(symbol_name_split[3])/1000

            if(exp == exp_short):
                strikes.add(strike)

        strikes = sorted(strikes)

        return(strikes)



    @staticmethod
    def dollars_to_int16(a):
        if(a is None):
            return(0)
        b = round(a*100)
        return(b)

    @staticmethod
    def int16_to_dollars(a):
        a = a.astype(np.uint16, copy=False)
        b = a/100
        return(b)

    @staticmethod
    def int16_to_uint16(a):
        b = a.astype(np.uint16, copy=False)
        return(b)

    @staticmethod
    def dec_to_int16(a):
        if(not -1.0 <= a <= 1.0):
            logging.warn(f"{a} is not between -1 and 1. Setting to zero.")
            a = 0
        b = round(a*32767)
        return(b)

    @staticmethod
    def int16_to_dec(a):
        b = a/32767
        return(b)


    @staticmethod
    def format_quote_entry(quote):

        quote_list = [
            round(quote['sec_of_day']),
            HDF5tocks.dollars_to_int16(quote['last']),
            HDF5tocks.dollars_to_int16(quote['ask']),
            HDF5tocks.dollars_to_int16(quote['bid'])
        ]

        quote_entry = np.array(quote_list, dtype='i2')

        return(quote_entry)

    @staticmethod
    def unformat_quote_entry(entry):

        quote = {
            'sec_of_day':HDF5tocks.int16_to_uint16(entry[0]),
            'last':HDF5tocks.int16_to_dollars(entry[1]),
            'ask':HDF5tocks.int16_to_dollars(entry[2]),
            'bid':HDF5tocks.int16_to_dollars(entry[3])
        }

        return(quote)

    @staticmethod
    def format_option_entry(option):

        option_list = []
        try:
            option_list = [
                round(option['sec_of_day']),
                HDF5tocks.dollars_to_int16(option['ask']),
                round(option['asksize']),
                HDF5tocks.dollars_to_int16(option['bid']),
                round(option['bidsize'])
            ]
            if('greeks' in option):
                option_list.extend([
                    HDF5tocks.dec_to_int16(option['greeks']['ask_iv']),
                    HDF5tocks.dec_to_int16(option['greeks']['bid_iv']),
                    HDF5tocks.dec_to_int16(option['greeks']['delta']),
                    HDF5tocks.dec_to_int16(option['greeks']['gamma']),
                    HDF5tocks.dec_to_int16(option['greeks']['mid_iv']),
                    HDF5tocks.dec_to_int16(option['greeks']['phi']),
                    HDF5tocks.dec_to_int16(option['greeks']['rho']),
                    HDF5tocks.dec_to_int16(option['greeks']['smv_vol']),
                    HDF5tocks.dec_to_int16(option['greeks']['theta']),
                    HDF5tocks.dec_to_int16(option['greeks']['vega'])
                ])
            else:
                logging.warn(f"No greeks for {option['symbol']}.")
                option_list.extend([
                    HDF5tocks.dec_to_int16(0),
                    HDF5tocks.dec_to_int16(0),
                    HDF5tocks.dec_to_int16(0),
                    HDF5tocks.dec_to_int16(0),
                    HDF5tocks.dec_to_int16(0),
                    HDF5tocks.dec_to_int16(0),
                    HDF5tocks.dec_to_int16(0),
                    HDF5tocks.dec_to_int16(0),
                    HDF5tocks.dec_to_int16(0),
                    HDF5tocks.dec_to_int16(0)
                ])
        except Exception as e:
            logging.error(f"Something when wrong with formating\n{option}")
            logging.error(e)
            time.sleep(3)

        logging.debug(f"Converting optionlist to dtype:i2  {option} {option_list}")
        option_entry = np.array(option_list, dtype='i2')

        return(option_entry)

    @staticmethod
    def unformat_option_entry(entry):

        option = {
            'sec_of_day':HDF5tocks.int16_to_uint16(entry[0]),
            # TODO: Add timestamp calc
            'ask':HDF5tocks.int16_to_dollars(entry[1]),
            'asksize':HDF5tocks.int16_to_uint16(entry[2]),
            'bid':HDF5tocks.int16_to_dollars(entry[3]),
            'bidsize':HDF5tocks.int16_to_uint16(entry[4]),
            'greeks': {
                'ask_iv':HDF5tocks.int16_to_dec(entry[5]),
                'bid_iv':HDF5tocks.int16_to_dec(entry[6]),
                'delta':HDF5tocks.int16_to_dec(entry[7]),
                'gamma':HDF5tocks.int16_to_dec(entry[8]),
                'mid_iv':HDF5tocks.int16_to_dec(entry[9]),
                'phi':HDF5tocks.int16_to_dec(entry[10]),
                'rho':HDF5tocks.int16_to_dec(entry[11]),
                'smv_vol':HDF5tocks.int16_to_dec(entry[12]),
                'theta':HDF5tocks.int16_to_dec(entry[13]),
                'vega':HDF5tocks.int16_to_dec(entry[14])
            }
        }

        return(option)


class StockEntry(NamedTuple):
    symbol: str
    datetime: datetime
    entry_date: str
    second_of_day: int
    last: float
    ask: float
    bid: float


    def validate(self):

        success = True

        if(not isinstance(self.symbol, str)):
            logging.debug(f"symbol isn't str. {self.symbol}")
            success = False
        if(not (len(self.symbol) <= 6 
            and self.symbol.upper() == self.symbol
            and self.symbol.split()[0] == self.symbol)):
            logging.debug(f"symbol not in correct format. {self.symbol}")
            success = False

        if(not isinstance(self.datetime, datetime)):
            logging.debug(f"datetime isn't datetime. {self.datetime}")
            success = False
            # TODO: Validate datetime is same as market_day and second_of_day

        if(not isinstance(self.entry_date, str)):
            logging.debug(f"entry_date isn't str. {self.entry_date}")
            success = False
        if(re.compile("([0-9]{4})-([0-9]{2})-([0-9]{2})").match(self.entry_date) is None):
            logging.debug(f"entry_date not in correct format. {self.entry_date}")
            success = False

        if(not isinstance(self.second_of_day, int)):
            logging.debug(f"second_of_day isn't int. {self.second_of_day}")
            success = False
        if(not 0 <= self.second_of_day <= 6.5 * 3600):
            logging.debug(f"second_of_day isn't in range. {self.second_of_day}")
            success = False

        if(not isinstance(self.last, float)):
            logging.debug(f"last isn't float. {self.last}")
            success = False
        if(not 0 <= self.last < 100000):
            logging.debug(f"last isn't in range. {self.last}")
            success = False

        if(not isinstance(self.ask, float)):
            logging.debug(f"ask isn't float. {self.ask}")
            success = False
        if(not 0 <= self.ask < 100000):
            logging.debug(f"ask isn't in range. {self.ask}")
            success = False

        if(not isinstance(self.bid, float)):
            logging.debug(f"bid isn't float. {self.bid}")
            success = False
        if(not 0 <= self.bid < 100000):
            logging.debug(f"bid isn't in range. {self.bid}")
            success = False

        return success


class MarketGreek(object):

    def __init__(self, metric):
        assert isinstance(metric, np.float16), metric
        self._metric = metric

    @property
    def float16(self):
        return self._metric

    @property
    def py_float(self):
        return float(self._metric)

    @staticmethod
    def from_float(metric):
        assert isinstance(metric, float), metric
        return MarketGreek(np.float16(metric))


class StockPrice(object):

    def __init__(self, cents):
        assert isinstance(cents, int), cents
        assert 0 <= cents < 4294967296, cents
        self._cents = cents

    @property
    def price(self):
        return self._cents

    @property
    def dollars(self):
        return self._cents / 100

    @staticmethod
    def from_dollars(dollars):
        assert isinstance(dollars, (float, int)), dollars
        return OptionPrice(cents=int(dollars * 100))


class OptionPrice(object):

    def __init__(self, cents):
        assert isinstance(cents, int), cents
        assert 0 <= cents < 65536, cents
        self._cents = cents

    @property
    def price(self):
        return self._cents

    @property
    def dollars(self):
        return self._cents / 100

    @staticmethod
    def from_dollars(dollars):
        assert isinstance(dollars, (float, int)), dollars
        return OptionPrice(cents=int(dollars * 100))


class MarketSize(object):

    def __init__(self, size):
        assert isinstance(size, int), size
        assert 0 <= size < 65536, size
        self._size = size

    @property
    def size(self):
        return self._size


class MarketSecond(object):

    def __init__(self, market_second, market_day, year):
        assert isinstance(market_second, int), market_second
        assert 0 <= market_second <= 23400, market_second
        self._market_second = market_second
        assert isinstance(market_day, int), market_day
        assert 0 <= market_day < 253
        self._market_day = market_day
        assert isinstance(year, int), year
        assert 1900 < year < 2100, year
        self._year = year

    @property
    def market_second(self):
        return self._market_second

    @property
    def market_day(self):
        return self._market_day

    @property
    def year(self):
        return self._year

    @property
    def date(self):
        cal = mcal.mcal_proxy.get_valid_days(f"{self.year}-01-01", f"{self.year}-12-31")
        date = cal[self.market_day]
        return date

    @property
    def time(self):
        return self.market_second + 9.5 * 3600

    @property
    def datetime(self):
        dt = datetime.strptime(self.date, "%Y-%m-%d") + timedelta(seconds=self.time)
        return dt

    @property
    def timestamp(self):
        return(self.datetime - datetime(1970,1,1)).total_seconds()

    @staticmethod
    def from_datetime(dt):
        assert isinstance(dt, datetime), datetime
        market_second = (dt - dt.replace(hour=9, minute=30, second=0)).total_seconds()
        market_day_num, market_day = mcal.get_market_day_of_year(dt)
        assert market_day, f"Is not a market day {dt}"
        return MarketSecond(market_second, market_day_num, dt.year)

    @staticmethod
    def from_timestamp(ts):
        assert isinstance(ts, (int, float)), ts
        dt = datetime(1970, 1, 1) + timedelta(seconds=ts)
        return MarketSecond.from_datetime(dt)


if __name__ == "__main__":

    opt = {
        "timestamp":time.time(),
        "symbol": "VXX190517P00016000",
        "description": "VXX May 17 2019 $16.00 Put",
        "exch": "Z",
        "type": "option",
        "last": None,
        "change": None,
        "volume": 0,
        "open": None,
        "high": None,
        "low": None,
        "close": None,
        "bid": 0.0,
        "ask": 0.01,
        "underlying": "VXX",
        "strike": 16.0,
        "change_percentage": None,
        "average_volume": 0,
        "last_volume": 0,
        "trade_date": 0,
        "prevclose": None,
        "week_52_high": 0.0,
        "week_52_low": 0.0,
        "bidsize": 0,
        "bidexch": "J",
        "bid_date": 1557171657000,
        "asksize": 611,
        "askexch": "Z",
        "ask_date": 1557172096000,
        "open_interest": 10,
        "contract_size": 100,
        "expiration_date": "2020-05-17",
        "expiration_type": "standard",
        "option_type": "put",
        "root_symbol": "VXX",
        "greeks": {
            "delta": 1.0,
            "gamma": 1.95546E-10,
            "theta": -0.00204837,
            "vega": 3.54672E-9,
            "rho": 0.106077,
            "phi": -0.28801,
            "bid_iv": 0.0,
            "mid_iv": 0.0,
            "ask_iv": 0.0,
            "smv_vol": 0.380002,
            "updated_at": "2019-08-29 14:59:08"
        }
    }
    quote = {
        "timestamp":time.time()-(86400*2),
        "symbol": "XYZ",
        "description": "Apple Inc",
        "exch": "Q",
        "type": "stock",
        "last": 208.21,
        "change": -3.54,
        "volume": 25288395,
        "open": 204.29,
        "high": 208.71,
        "low": 203.5,
        "close": None,
        "bid": 208.18,
        "ask": 208.20,
        "change_percentage": -1.68,
        "average_volume": 27215269,
        "last_volume": 100,
        "trade_date": 1557168406000,
        "prevclose": 211.75,
        "week_52_high": 233.47,
        "week_52_low": 142.0,
        "bidsize": 12,
        "bidexch": "Q",
        "bid_date": 1557168406000,
        "asksize": 1,
        "askexch": "Y",
        "ask_date": 1557168406000,
        "root_symbols": "XYZ"
    }

    logging.getLogger().setLevel(logging.DEBUG)
    a = HDF5tocks('a.hdf5')
    a.add_quote(quote)
    # a.add_option(opt)
    # print(a.get_option(opt['symbol']))
    # exp1 = ['2020-01-01', '2020-01-02', '2020-01-03']
    # exp2 = ['2020-01-04', '2020-01-03', '2020-01-07']
    # exp3 = ['2019-12-31', '2020-44-44', '4321-452-1']
    # a.update_expirations(exp1)
    # print(a.get_expirations())

